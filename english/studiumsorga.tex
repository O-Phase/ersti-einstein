\chapter{Study Organisation}
At the university you are now quite free to organise your own studies:
You can decide for the most part which events you attend and how, with whom and when you study.
On the other hand, especially in the first semester, panic easily builds up.
What do I have to visit when and where? Do I have to register somewhere?\\
But don't worry your checklist for the first few weeks is quite short

\begin{itemize}
    \item[( )] Select lectures
	\item[( )] Register for courses (e.g., on Moodle, LSF, Uni2Work)
    \item[( )] Attend first lecture
    \item[( )] Find lecture homepage
    \item[( )] Register in lectures / tutorials (if necessary)
    \item[( )] Rethinking the timetable
\end{itemize}

In the following you will find some tips for your study organisation which have proven their worth over generations of study programmes.

\section{Lectures and timetable} \label{vlvz}

Your personal timetable is one of the most important tasks,
that you have to take care of every semester.
There are two aspects to the timetable that we would like to explain to you: on the one hand the technical aspects, and on the other hand the content-related aspects of the creation.
For the first semester we will provide you with a sample timetable, However, you should keep in mind that it does not yet include exercises and minor lectures.
So don't underestimate that your timetable will look very empty at the beginning, because the gaps will fill up faster than you think.
Before we take care of the study plan, let's take a look at the course catalog.

\subsection*{Curriculum (Teaching, Studies, Research -- LSF)}

The LMU uses the so-called LSF as its online course catalogue, 
which provides an overview of (almost) all events at the LMU, including non-subject and interdisciplinary events such as language courses. 
It is updated toward the end of each semester for the following semester, so that from March you will find the events of the coming summer semester and August you will find the events of the upcoming winter semester, which you can enter in the timetable tool.

\subsection*{ECTS-points}
To be able to better estimate the scope of work of the courses,
the \textbf{ECTS points system} (sometimes also called credit points system) is used: \\
ECTS points measure the total time spent on the study course and include both attendance and self-study.
As a rule, 30 ECTS credits per semester are earned by attending and completing courses. 
A workload of 30 hours is assumed for one credit point. 
You can find out how many ECTS credits are earned through which modules in the \textit{Examination and Study Regulations}, which is something like a law book for your course of study.

\subsection*{Technical}
Creating the timetable is a bit cumbersome.
First you have to select the event you want to add,
find it in the LSF (\enquote{search for events}) and make a note of it
(there is a small box for this purpose, directly below the dates).
It will now be displayed in your timetable.
You should definitely save it and you can download it to your mobile phone or print it out as a PDF.
Because sometimes the tool does not do what you want it to do,
we have printed a template for you on page\pageref{stundenplan},
where you can enter your timetable by hand.

\subsection*{Contents}
Once you have found out \textit{how} you create your timetable,
the question naturally arises as to what you fill it with.
In order to answer this question, we have included study plans for you at the end of the Einstein where you can see which courses you have to take during your studies.
You are generally free to choose what you take and when, but it is advisable to attend the courses specified in the study plan for your semester.
In addition, we will give you a few more useful tips for creating your schedule:

First of all, make out all so-called \emph{compulsory courses} from your study plan.
These are the courses (see chapter ~\ref{lehrveranst}) which you must pass in order to receive your degree.
It is best to first plan all mandatory courses of the respective semester (keep in mind that some of them are only offered every two semesters) and only then take care of the other (compulsory elective) courses.
You should not spend more than 20 hours a week on lectures and seminars, so that
you have enough space to now enter exercises, tutorials and slots for preparation and follow-up.

Since there are many different courses of study at LMU, it can also be interesting, to attend events from other departments, language courses or other advanced courses.
You can find these in the LSF under \enquote{Additional Qualifications for Students}.

After the first or second week of lectures, you should check whether it is not too much for you.
Finally, you should draw up a semester plan, in which all important dates such as re-registration deadlines, examinations, presentations or preparation times for examinations are noted.



%You can also enter these in our calendar in the middle of the Einstein.
%TODO: Possibly delete/edit this sentence if the calendar has been adjusted.

\begin{urlList}
	\urlItem{http://www.lsf.lmu.de}[lsf]
	\urlItem{http://www.hilfe.lsf.uni-muenchen.de/lsf_hilfe/funktionen/stdplan/index.html}[timetable]
	\urlItem{https://www.lmu.de/de/studium/studienangebot/zusatzqualifikationen/}[additional qualifications]
\end{urlList}

	
%Create this here as a link list? don't leave it out
%\section{additional offers}
%\begin{itemize}
% \item foreign languages: \url{www.sprachenzentrum.lmu.de}
% \item lecture series: \url{www.lmu.de/ringvorlesung}
% \item Studium Generale: \newline \url{www.lmu.de/studium/studienangebot/lehrangebote/studium_generale}
% \item LMU PLUS seminars: \url{www.frauenbeauftragte.lmu.de/plus/plus_veranstaltungen}
% \item soft skills, job application training: \url{s-a.uni-muenchen.de} and \url{www.jobline.lmu.de}
% \item Soft Skills at the TUM: \newline \url{www.cvl-a.de/index.php?option=com_content&view=article&id=24}
%\end{itemize}


\section{Registration for tutorials, exams and extracts of grades}

Your lecturers usually announce at the beginning and towards the end of the semester in the lectures, whether and which registrations are necessary for the exercises and exams and when.\\
Depending on the course of study, different platforms are common:

\begin{itemize}
	\item Uni2Work \subjectList{\subjectI{}\subjectMI{}\subjectM{}}\\
	Informatics and Media Informatics students can use Uni2Work for starting workflows, such as for the bachelor's thesis. For some external exams you will also get your grades on Uni2Work.
	Uni2Work can be accessed with the campus or CIP ID. It is also used in some mathematics lectures.
	\item Moodle \subjectList{\subjectI{}\subjectMI{}\subjectM{}\subjectP{}}\\
	Here, you can enroll in various modules, internships and seminars. Moodle is used across subjects for exams, grades, assignment of exercise sheets, submission of assignments, lecture-related materials, announcements, or orientation phases. You can log in using your campus ID. \ref{moodle}
	\item LSF \subjectP\\
	Physics students often use LSF for registrations and allocations for exercises and exams. The times for exam registration are normally announced in the lectures.
	\item Own Websites\\
	Some instructors use their own websites for exercise and exam registration. Pay attention to announcements in the initial lectures.
	\item Lecture Websites\\
	To find out where the lecture websites are for mathematics courses, the Lecture (Websites) page of the Mathematical Institute \ref{mivw} is very practical. Here, all lecture websites for the current semester are linked.
	Similarly, the Institute of Computer Science usually provides links to lecture websites on their course offerings website \ref{ifilehre}. Sometimes it takes a little while for all of them to appear there, but generally, everything is available in time for the start of lectures.
\end{itemize}

At the end of the semester you can obtain your grade statement (often also called \textit{transcript} or \textit{account statement}). 
In most cases, the current grade statements are only available after your last exams, with some delay.
Here too, there are different ways of accessing the grade statement, depending on the degree programme:
\begin{itemize}
	\item PVI and Exam Results in Computer Science \subjectList{\subjectI{}\subjectMI{}}
	As a (media) informatics student, you usually receive your exam results through Uni2Work, Moodle, postings, or by other means (this largely depends on the specific course you are taking).
	Currently, there are plans to relocate the examination office, so there may be some changes in this regard.
	In the Examination Administration and Information System (see PVI \ref{pvi}), you can find your transcript with all your grades.
	However, please be prepared for it to take up to a month for grades from other platforms to be posted there.
	\item LSF \subjectP\\
	Physics and mathematics students under the examination regulations (PStO) of 2021 can also generate their current transcript online on LSF.
	\item Examination Office and Contact Point \subjectM\\
	Mathematics students under older examination regulations (PStOs) can request their current transcript to be sent to them via email (see \ref{pam}). However, it may sometimes take until the next semester for all examination offices to process it.
\end{itemize}

\begin{urlList}
	\urlItem{https://moodle.lmu.de/}[moodle]
	\urlItem{https://uni2work.ifi.lmu.de/}[uni2work]
	\urlItem{https://www.mathematik.uni-muenchen.de/studium/webseiten/vorlesungswebseiten/index.shtml}[mivw]
	\urlItem{https://www.ifi.uni-muenchen.de/lehrangebot/index.html}[ifilehre]
	\urlItem{http://pvineu.ifi.lmu.de}[pvi]
	\urlItem{https://www.pani.pa.uni-muenchen.de/kontaktstellen/kontaktstelle-mathematik/index.html}[pam]
\end{urlList}


\section{Useful online services of the LMU}
\label{sec:online}

\begin{minipage}{6cm}
\subsection*{Campus LMU}

Here you can activate your campus ID, get access to your e-mail account,
your user account and the course catalogue (LSF) and you can subscribe and unsubscribe from LMU newsletters.

\begin{urlList}
	\urlItem{http://www.portal.lmu.de}
\end{urlList}
\end{minipage}
\begin{minipage}{7cm}
\begin{center}
	{\includegraphics[width=0.6\textwidth]{comic_university}}
\end{center}
\end{minipage}

\subsection*{Online self-service functions}
\label{Immbest}
Certificates of enrolment, course of studies and paid fees as well as the form for enrolment for exams can be found here.
These are available online at any time, which is useful for employment contracts.
You can also change your address details and telephone numbers.

\begin{urlList}
	\urlItem{https://www.lmu.de/de/workspace-fuer-studierende/1x1-des-studiums/online-selbstbedienungsfunktionen-fuer-lmu-studierende/index.html}
	\urlItem{https://www.lmu.de/de/studium/wichtige-kontakte/studentenkanzlei/index.html}
\end{urlList}

\section{Libraries}\label{bib}
The libraries are an important part of the study programme as places of learning and as providers of various useful research services and book lending. In the following you will find 
a short introduction to the services of libraries.

\subsection*{Books}

If you have difficulties understanding the material, it not only helps to ask your fellow students for advice, but also to read books.
The library has a large stock of literature, some of which you can also borrow.
As a rule, the books recommended by the Professorika are available several times over, although they often go out of stock quickly.
If a book you need is not available:
Purchase requests will be fulfilled within approximately one month.

\subsection*{E-Media}
Numerous e-books, papers and scientific journals from well-known scientific publishers are available to LMU members free of charge.
If you visit the websites of the relevant publications, the publishers will ask you to buy them.
However, if you visit the website via the University Library's E"~Media Login~\ref{emedien},
the works are usually available for free download. 
\begin{urlList}
	\urlItem{http://emedien.ub.uni-muenchen.de/}[emedien]
\end{urlList}

\subsection*{Research in OPAC and borrowing}

Books in the central library and other specialised libraries can almost all be borrowed. 
In the case of reference libraries, borrowing is only possible over the weekend. 
If you are looking for a specific book, you can search for it in the OPAC~\ref{verlängerung}, where you will find the place where the book is located.
Through the OPAC, you can also find e-books or papers. If you encounter difficulties in your search, you can attend a tutorial offered by the university library to learn all the tips and tricks of library usage (see \ref{bibtut}).

Please note the loan periods (reminder fees vary depending on the library)!
Extensions are available possible under ~\ref{verlängerung} 
(\textit{Your Account} $\rightarrow$ \textit{Checked Out Items})
 -- provided you do not have any outstanding reminder fees.

Fees can be paid via ePayment or at the cash machine in the main building.



\begin{urlList}
	\urlItem{https://opac.ub.lmu.de}[verlängerung]
	\urlItem{http://www.ub.uni-muenchen.de/kurse/index.html}[bibtut]
\end{urlList}

\subsection*{Behaviour in the library}
Prohibited depending on the library:
Smoking, food, drinks (except water in transparent bottles), coats, jackets, bags (including laptop bags), mobile phone ringing, conversations.

Prohibitions vary relatively widely by library.
Please inform yourself about them online beforehand.
The libraries are also often used as a quiet place to study. Therefore:
\textbf{Please behave quietly!
Your learning commillitonica will thank you for it.}

In order to be able to continue to use the library well -- please forgive us this preventive moral cudgels --,
it would be fair to the other students,
if you especially occupy the lockers in front of the library for the duration of your stay in the Bib.
Lately, it has become the exception to find a locker at all for a short visit to the library, as many keep the key permanently and thus block the locker for a longer period of time.

\subsection*{The most important libraries for you}

%% UPDATE 2024: Update opening hours and equipment, check everything
\subsection*{Library of Mathematics and Physics\subjectList{\subjectM\subjectW\subjectP}}
Theresienstraße 37 (1st floor)\\
Opening hours: Mon-Fri 8:00--22:00~h, Sat 10:00--20:00~h\\
Accessible entrance, book scanner, copier/scanner with card payment, WLAN, group study rooms, LMUcard charging station, basic library for all students of faculties 16/17.

\subsection*{Library of Economic Sciences and Statistics}
Ludwigstraße 28\\
Opening hours: Mon-Fri 8:00--22:00~h, Sat 10:00--20:00~h\\
Accessible entrance, book scanner, copier/scanner with card payment, WLAN, group study rooms, LMUcard charging station.

\subsection*{Fachbibliothek Englischer Garten\subjectList{\subjectI\subjectMI}}
Oettingenstraße 67 (main entrance, ground floor)\\
Opening hours: Mon-Fri 9:00--20:00~h, Sat 10:00--20:00~h\\
Accessible entrance, book scanner, copier/scanner with card payment, WLAN, group study rooms, on-site computer science library.

\subsubsection*{Lernzentrum Leo 13\subjectList{\subjectI\subjectMI}}
Leopoldstr. 13, Building section 1\\
Opening hours: Mon-Fri 9:00--20:00~h\\
Individual and group workplaces\\
Accessible entrance, photocopier, book scanner.

\subsection*{Central Library of the LMU}
Ludwigstraße 27 (main building south wing)\\
Self service return machine: Mon-Fri 6:30--22:00~h, Sa 8:00--22:00~h, So 8:00--20:00~h\\
Service desk: Mo--Fr 9:00--17:00~h\\
Contact point for lost library cards and for the collection of books from the central stock, as well as for inter-library loans.

\subsection*{Library of the TUM in the city centre}
Arcisstraße 21 (main building TUM, 1st floor)\\
Opening hours: Mon-Fri 8:00--24:00~clock, Sat-Sun 09:00--22:00~clock\\
Free to study for all students, a TUM library card can be obtained upon presentation of the student card at the information desk.

\begin{urlList}
	\urlItem{http://www.ub.tum.de}
\end{urlList}

\subsection*{Bayerische Staatsbibliothek (Stabi)}
Ludwigstraße 16\\
Local lending hours: Mon-Fri 10:00--19:00~h\\
General reading room hours: Daily (including Sundays!) 8:00--24:00~h\\
Magazine reading room hours: Mon-Fri 9:00-20:00~h, Sat 9:00--17:00~h\\
Slots have to be booked in advance.
Huge stock (sheet music, journals, antiques, \ldots), books must be ordered online, borrowing with your LMU library card.
If you want to get a job, be there early; the rush of people willing to learn is immense.
However, there are also relatively strict conditions of use.
For example, the guard might get nervous if you arrive in large groups.
Only water in transparent bottles may be brought along for drinking.

\begin{urlList}
	\urlItem{http://bsb-muenchen.de}
\end{urlList}

\subsection*{Bibliothek des Deutschen Museums}
Museum Island 1\\
Opening hours: Daily (also Sundays!) 9:00--17:00~Uhr\\
Large selection of technical and scientific works, reference library, beautiful building.

\begin{urlList}
	\urlItem{https://deutsches-museum.de/bibliothek}
\end{urlList}

\subsection*{Munich City Library (Several offices all over the city)}
City Library at Motorama, Rosenheimer Straße 30-32\\
Opening hours: Mon-Sat 7:00--22:00~h, Sun 10:00--20:00~h\\
Service hours: Mon-Fri 10:00--19:00~h, Sat 10:00--15:00~h

City Library at HP8, Hans-Preißinger-Straße 6\\
Opening hours: Mon-Sat 7:00--23:00~h\\
Service hours: Mon-Fri 10:00--19:00~h, Sat 10:00--15:00~h

Branches spread all over the city, loan for Studika \EUR{10,00}~per year.

\begin{urlList}
	\urlItem{http://www.muenchner-stadtbibliothek.de/}
\end{urlList}

\section{Study Abroad}\label{Auslandssemester}

Semesters abroad always look good on your CV and leave behind lasting memories,
from which many of us have profited more than from one or the other lecture.
And if you are particularly interested in one subject, many universities abroad also offer the opportunity to write a thesis with them.

An initial overview of resources regarding semesters abroad can be found on our student association's website:
\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de/rund-ums-studium/auslandsstudium}
\end{urlList}

When organizing a semester abroad, the university's internal \emph{Office for International Affairs} offers assistance.
If you are interested in a thesis or an internship abroad, student organizations like AIESEC\footnote{Association Internationale des Etudiants en Sciences Economiques et Commerciales} or IAESTE\footnote{International Association for the Exchange of Students for Technical Experience} (sponsored by DAAD\footnote{Deutscher Akademischer Austauschdienst}) are good contact points.
\subsection*{Referat International Affairs}
Ludwigsstraße 27 near the main building, G009 and G011

\begin{urlList}
	\urlItem{http://www.lmu.de/international/auslandsstudium}

\end{urlList} 

The LMU has a number of partner universities around the world.
Exchange tends to be easier here (formalities, recognition of ECTS).
You can only apply once a year for the partner universities,
so it is best to find out about deadlines and register early.
One year before departure is sometimes already too late, to apply to all organizations (especially DAAD).
However, it is also possible to organise an exchange at another university yourself.

If you would like to have ECTS gained abroad recognised at the LMU,
you should clarify this in advance with the programme coordinator.



\begin{urlList}
	\urlItem{https://lmu.adv-pub.moveon4.de/home-page-2558/}[ausland]
\end{urlList}

\newpage
\subsection*{Financing}

This is only a selection of funding opportunities.
Special financial support is also available for certain countries and projects.
The lead time is 3-18 months.

\begin{itemize}
    \item Auslands-BAföG: State financial support (non-repayable) for studies or internships abroad. Many people who do not receive regular BAföG are also eligible for this support 
    -- so make sure to apply 6 months before your stay abroad!
    \item ERASMUS: A scholarship programme for a 3-~to 12"~month study or internship in another European country.
    \item DAAD and PROSA LMU: Scholarships for studies, internships, language courses and short programmes abroad.
\end{itemize}
\begin{urlList}
	\urlItem{https://www.lmu.de/de/workspace-fuer-studierende/auslandserfahrung-sammeln/auslandsstudium/austauschprogramme/erasmus/index.html}
	\urlItem{https://www.lmu.de/de/workspace-fuer-studierende/auslandserfahrung-sammeln/auslandsstudium/finanzierung/prosa/index.html}
	\urlItem{https://www.daad.de/ausland/de/}
\end{urlList}




