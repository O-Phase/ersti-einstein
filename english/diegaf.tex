\chapter{Fachschaft -- The Student Association} \label{diegaf}

% HINWEIS 2023: Wir (Yannick, Noah) haben uns auf folgende Übersetzungen geeinigt:
% Fachschaft = Student Association
% StuVe = Student Body
% Studierendenwerk = Studierendenwerk
% Konvent der Fachschaften = Council of Student Associations

\section{What is the Fachschaft?}

A student association is basically a community of all students of a study programme -- so each of you is a member.

\begin{minipage}{6.5cm}
When we talk about \emph{the student association} here, however, we usually mean the Gruppe Aktiver Fachschaftika, GAF for short.
This is a group of students who are active in various fields, who are committed to make the university more pleasant for themselves and other students.
The GAF brings together the active students from the field of mathematics,
physics and computer science, as well as related subjects (media informatics, business mathematics, \ldots ).
\end{minipage}
\begin{minipage}{6cm}
\begin{center}
{\includegraphics[width=0.7\textwidth]{fachschaft}}
\end{center}
\end{minipage}\\

The GAF is therefore the contact point for all study-related questions and needs, but also if you would like to get involved yourself.

\section{What does the student association do?} \label{gafaktionen}

The tasks of the student association can be roughly divided into three areas: Events, university politics and direct support of students.
Some things are immediately noticeable in everyday life, such as the festivals in the Maths Department.
Others remain hidden in the background, but all areas are important for university life to be as pleasant as possible and to preserve the rights of the Students.
In the following you will find a small insight into our student association work.

\subsection{Events}

The GAF organises various events of student life -- the first and most important, the \emph{O-Phase} for first semester students, you are probably already aware of.
This is where new students are introduced to the university and its degree programmes and gain insights into the unfamiliar surroundings.
But there's also often a lot to do during the semester:

\begin{itemize}
\item At the \emph{faculty festival} the students come together and have a barbecue together in summer in front of the maths building.
\item The \emph{Profcafé} gives the opportunity to talk to Professor in a more relaxed atmosphere with cheese and wine.
\item At the \emph{Long Night of the Universities}, Professor give interesting and amusing lectures about their departments until the morning.
\item The \emph{Games Evening}, the \emph{Sheep's Head Tournament} or the \emph{Dance Evening} are perfect for getting to know other students and spending some relaxed time.
\item \ldots
\end{itemize}

\subsection{Higher education policy}

Another important task of the student association is to give the student community of its degree programmes a voice in higher education policy.
For example, there are various committees at our two faculties in which important decisions are made.
Every summer semester, the university elections take place, where you can vote for the student representatives who will represent you. The student associations, thus elected, then send students to various university committees. These students then have a say in the decision-making
of these committees, lend weight to the opinions and interests of the Students and thus try to prevent decisions from being made over their heads.

We have listed some of the most important committees here:

\begin{itemize}
\item The \emph{faculty council} decides everything important within the faculty and is the place of information exchange between professors, scientific assistants and us students.
\item In the \emph{teaching commissions} Studika work out recommendations for the improvement of teaching and studies.
\item The \emph{appointment commissions} determine who will come to our university as a new professor and also teach here.
\item The \emph{Studienzuschusskommissionen} decide how the funds from study grants for the improvement of teaching and studies should be allocated.
\item The \emph{Council of the Student Associations} consists of representatives of all student associations and deals with interdisciplinary student topics such as the semester ticket.
\end{itemize}


\begin{figure}[h!]{
\begin{center}{

\resizebox{\linewidth}{!}{

\begin{tikzpicture}[scale=0.9]
\pie[sum=auto, radius=1, color={yellow!20, red!50}] {14/, 2/}
\node[above] at (0,1) {Fakultätsrat};
\pie[pos={3,0}, sum=auto, radius=1, color={yellow!20, red!50}]{6/, 6/}
\node[above] at (3,1) {Studienzuschuss};
\pie[pos={6,0}, sum=auto , after  number=, radius =1, color={yellow!20, red!50}]{10/, 4/ }
\node[above] at (6,1) {Lehrkom.};
\pie[pos={9,0}, sum=auto, radius=1, color={yellow!20, red!50}, text=legend]{8/ other interest groups, 1/ voting studios}
\node [above] at (9,1) {Commission on appointment};
\end{tikzpicture}
}\caption*{Share of students with voting rights in the committees within the faculty}
}\end{center}
}\end{figure}


\subsection{General Assistance}

In general, the GAF also tries to make university life as easy as possible for the students.
We are available as contact persons for problems of any kind, 
in the student association room, by e-mail or telephone (see section \ref{gafKontakt}).
No matter whether you have problems in lectures, have any questions about your studies or suggestions on how to improve your studies:
Just come by or get in touch.

\subsection{Old exams and scripts collection}

A very popular service of the GAF is the collection of old exams and oral examination protocols -- at the latest in the first exam period, students learn to appreciate them for exam preparation.
You can find the largest part of the collection in our online collection~\ref{klausuren}.
You can ask for your username and password at the GAF or contact us via E"~Mail~\ref{daten}.

On our collection page you will also find notes on lectures, examination records for the state examination and the intermediate examination, as well as old records from the diploma period.
It is important that we only collect self-written notes, and especially no official scripts of the teachers.

When collecting, the intergenerational contract must also be taken into account: it only exists because older students have brought their examinations and scripts to us,
so if possible, do the same for your successors, so that our archives are as complete as possible.
Just send everything you can get your hands on, if not yet available, to \mail{klausuren@fs.lmu.de}.

Even if the professors collect the information again after the exam, you have the right to take a photograph of your exam at the inspection appointment later.
If you are denied this right, you can refer to the ministerial decision \enquote{further development of the Bologna Process, in particular examination rights},
which explicitly allows the photographing of your exam. The next ones will thank you for it! 

\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de/klausuren}[klausuren]
	\urlItem{https://gaf.fs.lmu.de/rund-ums-studium/zugangsdaten}[daten]
\end{urlList}

\subsection{The Café Gumbel}

Another important part of the student association work is the operation and maintenance of Café Gumbel.
This is a student lounge with kitchen at Theresienstraße 37-39 (Room B\,030).

Café Gumbel, which was struck by Studika decades ago, has been managed by the student association for several years now, which in turn makes it available to Students.
In addition to tables for working, comfortable sofas for relaxing and a piano, the café also has a kettle and microwave at its disposal.
Of course there are rules in the kitchen regarding order and cleanliness as well as the careful use of cupboards and appliances.
Here it is important to leave everything as you would also like to find it and treat other people's property with care.
Should there be a problem, please report it to the student association so that we can take care of it.

Since the Gumbel is the pivot for many students, some personal belongings are forgotten here and in other university rooms.
To make sure that you have a chance to get your belongings back, it's best to label everything with your name and phone number. If something should get lost, it is best to ask in the maths department
at the gate or in the student association room, where lost items are occasionally washed up.

From time to time, Café Gumbel is also used for student events, some of which are already listed under Events, such as the games evening.
If you have any questions about the Gumbel or events in the Gumbel, you can reach the responsible persons via \mail{gumbel@fs.lmu.de}.

\begin{minipage}{6cm}
	 \textit{The Café Gumbel is, by the way, named after the Munich mathematician and political activist Emil Julius Gumbel, who dealt with statistics on politically motivated murders and was active against National Socialism.}
\end{minipage}
\begin{minipage}{7cm}
\begin{center}
     \includegraphics[width=0.8\textwidth]{gumbel}\\\
     Our student café
\end{center}
\end{minipage}

\section{Join the student association}

If you've become curious and want to take a look at the work of the student association and behind the scenes at the university, have good ideas for events yourself, help and co-organize at events or get an insight into higher education policy, you are always welcome at the GAF!

There are various working groups and areas in which you can participate, there is something for everyone. Of course it is
voluntary work; the money we have at our disposal is therefore only used for the benefit of the Students and the only wages are more
life experience and in some cases extended studies.

\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de/fachschaft}
\end{urlList}

\subsection*{The freshman weekend}

The freshman weekend, EWO for short, is an event especially for first-time students, at which you can spend a weekend together with experienced GAFika and new Students
travel, get to know each other, have fun and gain insights into the work of the student association.
It is the perfect opportunity to make contacts and get a taste of new areas.
If you are interested, it is best to register directly, because there are not always enough places.

\subsection*{The student association meeting}

Every two weeks, the student association meeting takes place, where GAF members and interested parties meet to discuss and make decisions on currently important topics. Newcomers are always welcome to join and are happy to explain what's currently on the agenda. Just come by!

\subsection*{Topic Meetings}

In addition to the student association meeting, topic meetings are held every Thursday and every second Monday, where various topics are discussed and worked on. These topic meetings are also a bit less rigidly structured than the student association meeting.
So if you want to get to know the student association's work, they offer a relaxed atmosphere for it.
The exact dates are always announced in our newsletter, and the location is always B038 in the Math building (Theresienstr. 39).

At the beginning of the semester, there will also be an introductory meeting where the different areas of student association's work will be presented.

Below, we have listed the different thematic areas of student council work. All of these are regularly worked on during the topic meetings.

\subsection*{O-Phase Organization}

If you had a lot of fun during the O-Phase or noticed things that you would do differently or much better, then the O-Phase working group (AK O-Phase) is the right place for you. Since the O-Phase is our biggest event, we start preparing immediately after the last O-Phase. The organizational meetings for the O-Phase usually take place every two weeks on Mondays, alternating with the student association meeting.
Here, you can help ensure that future generations of first-year students have a good start to their studies and simultaneously gain experience in planning and implementing large-scale events.

Questions can be sent to \mail{ophase@fs.lmu.de}.

\subsection*{University Politics (HoPo)}

Regularly, there are specific topic meetings focused on particular subjects (Math, (Media)Informatics, and Physics) or interdisciplinary HoPo sessions. In these, current topics and problems are discussed, and further action is planned. Common topics include university political committees, problems with certain modules or lecturers, communication with professors, improving the study situation, and much more. If you're interested in getting involved in this area, just come to one of these topic meetings.

\subsection*{Festivals and Events}

As mentioned above, GAF organizes many different events besides the O-Phase. If you'd like to help with an event or organize one, just come by. Suggestions and ideas for new events are also always welcome.

Questions can also be sent to \mail{gaf@fs.lmu.de}.

\subsection*{Communication}

To reach you, we use many different communication channels such as the ``Klostein'', the newsletter, or our social media accounts.

\subsection*{AK root}

In the AK root, people interested in technology and computer science can enjoy themselves. It takes care of the servers and services of the GAF and some other student bodies, such as
the mailing lists or the student association homepage, as well as the maintenance of the hardware and, in the case of occasional hacking nights, also larger IT topics.

If you are interested simply send a mail to \mail{root@fs.lmu.de}.

\section{Contact}\label{gafKontakt}

\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de}[gaf]
\end{urlList}

\begin{figure}[h]
	
	{\includegraphics[width=0.9\textwidth]{GAF_Gruppenfoto_2023}}
	
	groupepicture
	%% UPDATE 2024: aktuelles Fachschaftsbild
	
\end{figure}

\begin{figure}[h]

\begin{tabular}{|l l| l l}
\hline	
Zimmer&Theresienstraße 37\\
&Raum B038\\
&\\
Telefon&089 / 2180\emd{}4382\\
Telefax&089 / 2180\emd{}99 4382\\
&\\
&\mail{gaf@fs.lmu.de}\\
&\\
&\url{\https gaf.fs.lmu.de}\\
&\url{\https facebook.com/gaflmu}\\
&\url{\https instagram.com/gaflmu/}\\
&\\
IRC & \#gaf auf libera.chat\\ \hline
\end{tabular}

\end{figure}





