\chapter{University Organisation}


Welcome to the Labyrinth University! What is a university? It is a structure that has grown for 800 years, 
has existed in Munich for almost 200 years and consists of more than 800 professors, 14,000 employees and over 50,000 students.
Are you surprised that the organisation is not always immediately understandable?



\section{Structure of the University}

There are 18 faculties at the LMU that cover the spectrum
of humanities and cultural sciences, law, economics and social sciences, medicine and natural sciences. 
For the implementation of teaching and research, the faculties are supported by central administrative institutions. 
The university as a whole is subject to the university management, which is led by the Presidential Board. The President
is supported by five Vice Presidents who are responsible for areas such as studies.

\begin{center}
{
\resizebox{8cm}{!}{

    \begin{tikzpicture}[scale=1.4]

    \draw [dashed, thick, fill=white] (0,2) rectangle (10,-5);
    \node at (5,1.5) {\LARGE Heads of University};
    
    \draw [thick] (0,1) -- (10,1);
    \draw [thick]  (0,0) -- (6,0);
    \draw [thick] (6,-5) -- (6,1);


    \node at (3,0.5) {\LARGE Faculties};
    \node [text width=6 cm,align=center] at (8,-2) {\LARGE Central Administrative \newline Facilities};
    
    \node [rotate=90] at (3,-1.5) {\LARGE ...};
    \node [rotate=90] at (3,-4.5) {\LARGE ...};
    
    \draw [fill={rgb:orange,1;yellow,2;pink,5}, opacity=0.5] (0,-2) rectangle (6,-3);
    \draw [fill={rgb:red,1;green,3.5;blue,0}, opacity=0.2] (0,-3) rectangle (6,-4);
    
    \node [anchor=west] at (0.5,-3.5) {17 Faculty of Physics \subjectP };
    \node [text width=10cm, anchor=west] at (0.5,-2.5) {16 Faculty of Mathematics, Computer Science \\ and Statistics \subjectM \subjectW  \subjectI \subjectMI};


    \end{tikzpicture}

}}
\end{center}




\subsection*{Main Central Facilities}
During your studies you will get to know some of the central institutions. 
The \textbf{Studentenkanzlei} is responsible for central administrative matters such as 
Registration. 
You can find further information in chapter \ref{Immbest} and \ref{studikanzlei}.\\


If you are interested in spending a semester abroad, then Section III.3 for \textbf{International Affairs} can help you. 
You can find more information on page ~\pageref{Auslandssemester}. \\

The Department III.1 \textbf{Central Student Advisory Service} advises and supports students seeking interdisciplinary advice before and during the commencement, implementation and completion of a course of study as well as the transition to further studies or 
professional life. 
Within your faculty/institute you will also find a study guidance service, 
who will assist you in detail with questions about your studies.

\begin{urlList}
	\urlItem{https://www.lmu.de/de/studium/wichtige-kontakte/zentrale-studienberatung/}
\end{urlList}
\\ 

The \textbf{Universitätsbibliothek} consists of the 14 specialist libraries of the LMU Munich and also offers courses, 
guided tours, online service and numerous learning places in their locations. You can find more information on page~\pageref{bib}. \\ 

The \textbf{Münchner Zentrum für Lehrerbildung} (MZL) offers numerous counselling and further training opportunities. 
In particular, it can help you with interdisciplinary topics such as course enrolment, study structure, admission work and changing subjects.

\begin{urlList}
	\urlItem{https://www.mzl.uni-muenchen.de/index.html}
\end{urlList}

\subsection*{The Faculties}

The most important faculties for us are Faculties 16 for Mathematics, Informatics and Statistics and 17 for Physics.
Each faculty in turn consists of institutes. The division into institutes is not only based on subject, but also, as is so often the case, on historical factors.\\
Each faculty is headed by a dean, who is elected by the professors. 
This dean is largely freed from research and teaching in order to devote himself to the networking of the faculty and the university management as well as to faculty-internal development. 
The Dean's Office regularly reports on the most important innovations within the Faculty Council, in which Studika is also represented.


\begin{center}
{
\resizebox{\linewidth}{!}{
    \begin{tikzpicture}
\draw [fill={rgb:orange,1;yellow,2;pink,5}, opacity=0.5] (0,2) rectangle (9,-3);
\node [text width=9cm, align=center] at (4.5,1.5) {\large 16 Faculty of Mathematics, Informatics and Statistics};

\draw [thick] (0,1) to (9,1);
\draw (3,1) to (3,-3);
\draw (6,1) to (6,-3);

\node [text width=3cm, align=center] at (1.5,-1) {Mathematical\\Institute \\ \subjectM \subjectW};
\node [text width=3cm, align=center] at (4.5,-1) {Institute of \\Computer Science \\ \subjectI \subjectMI};
\node [text width=3cm, align=center] at (7.5,-1) {Institute of \\Statistics \\};


\draw [fill={rgb:red,1;green,3.5;blue,0}, opacity=0.2]  (10,2) rectangle (19,-3);
\node at (14.5,1.5) {\large 17 Faculty of Physics \subjectP };


\draw [thick] (10,1) to (19,1);
\draw (13,1) to (13,-3);
\draw (16,1) to (16,-3);


\node [text width=3cm, align=center] at (11.5,-1) {Meteorological Institute};
\node [text width=3cm, align=center] at (14.5,-1) {University Observatory};
\node [text width=3cm, align=center] at (17.5,-1) {Physics:\\ \small experimental physics,\\theoretical physics,\\didactics of physics};

\end{tikzpicture}

}}
\end{center}

Within the institutes, research and teaching is divided into professorships and working groups, which are subordinate (at least formally) to the professorships. Courses are often organised and realised from a chair. If you are interested in the more detailed research fields of your faculty (e.g. with regard to a thesis), it is worth looking for a list of chairs. Fortunately, we have made your work easier:

\begin{urlList}
	\urlItem{http://www.mathematik.uni-muenchen.de/forschung/arbeitsgruppen/index.html}
	\urlItem{https://www.physik.lmu.de/de/ueber-die-fakultaet-fuer-physik/einrichtungen/index.html\#st_text__master_1}
	\urlItem{https://www.ifi.lmu.de/forschung}
\end{urlList}



\section{Courses}\label{lehrveranst}

Different types of courses are held at a faculty.\\


\textbf{Lectures} are held by persons with teaching authorisation (this can be professors, private lecturers or academic employees).
Typically, these are frontal lessons with blackboard writing or presentation slides in front of several hundred students. 
Lectures are the main form of knowledge transfer in our subject group and require some preparation and follow-up work, so don't be surprised if you don't understand everything right away.
They end at the end of a semester with a final examination, usually a written exam.  \\

\textbf{Exercises and Tutorials} are conducted by experienced students or academic staff members. In tutorials (also known as exercises), the material from the lecture is reviewed and applied in smaller groups, while in central exercises (also known as exercises), the same is done for all course participants (i.e., not in small groups).
Usually, one central exercise and several tutorials are offered. In terms of content, exercise sheets are usually discussed, and separate shorter assignments are worked on together. What is done in the central exercise and what is done in the tutorials may vary. \\

\textbf{Seminars} consist of various presentations on a specific topic. Each participating student gives at least one of these presentations. \\

\textbf{Internships}, e.g. laboratory or programming internships, are events in which you can get directly involved, 
to practise programming or scientific work and experiments accompanied by experienced students. 
In some degree programmes, industrial/company internships can also be included.

\begin{itemize}
\item Programming practicals\subjectI \subjectMI \\
In computer science and media informatics, there are programming practical courses in addition to the other courses,
in which you develop software in groups with fellow students during the semester. 
\item laboratory practicals\subjectP \\
These are available for all students with a minor in experimental physics and physics
Bachelor's programmes (including meteorology and astrophysics).
You can find more detailed information in the LSF (see \ref{LSF}), including registration deadlines, and normally in an announcement during the respective experimental physics lectures.
\end{itemize}

Other \textbf{courses} take place during the semester or as a block course during the semester break.
Please note that you cannot necessarily receive credit for all courses.
\begin{itemize}
\item Computer courses are offered by the faculties, e.g. programming courses in physics or \LaTeX courses in mathematics.
\item Language courses are offered centrally by the Language Centre. 
You can find a list of courses and registration online in the course catalogue (see page \pageref{vlvz}).
\item Reading courses are designed for guided self-study of a textbook or a research article.
\end{itemize}

\begin{urlList}
    \urlItem{http://www.lsf.lmu.de}[LSF]
\end{urlList}

\section{Persons and salutations}
During the first weeks at university you will already meet many different people. Here is a selection of typical names and how to address your fellow human beings correctly:\\

\subsection*{Professorika}
were appointed at the university to pursue oriented research and teaching.
\begin{itemize}
\item \textbf{Classification:} Chair holders (W3-Professorika) head a chair and are usually very well-known researchers. 
In their field of research they are supported by associated professors (usually W2 or W1 professors). The prefix W1/W2/W3 indicates the salary level. 
These professorships are filled by so-called appointment commissions, in which, among other things, Studika examine candidates for research and teaching. 
If a professorship becomes vacant at short notice, a deputy professorial chair is convened without such a procedure. 
\item \textbf{Tasks:} Professorial staff not only fulfil their teaching obligations (in which they enjoy teaching freedom), but also carry out university research. This includes not only the holding of lectures but also the supervision of doctoral theses and student theses.
\item \textbf{Where to find:} You can find them mainly in lectures and their office hours.
\item \textbf{Form of address:} Usually you do not address professors by their full title, but with \enquote{Mr./Madam Professor \textless Surname\textgreater}. More common (also in writing) in our subject groups, however, is \enquote{Mr./Mrs. \textless surname\textgreater}.
Oh, and don't be surprised if you get something like \enquote{Ok, thanks.} as a quick reply.
Another official form of address for chair holders is \enquote{Ordinarius}. 
By the way, if you encounter a dean, you will certainly be able to surprise with the historically correct but obsolete form of address \enquote{Eure Spektabilität}.
\end{itemize}

\subsection*{Scientific employees} are employed at chairs and scientific working groups and
and are sometimes also called \textit{academic Mittelbau}. 
\begin{itemize}
\item \textbf{Classification:} In many cases, these are young scientists working towards their doctorate (doctoral candidates) 
or work as postdocs in chairs and working groups after their doctorate. 
Some of the research assistants are (temporarily) civil servants and can (e.g. as private lecturers) offer their own lectures or conduct research in their own working groups. In addition, there are also collaborators in the administration and organisation of the programme.
\item \textbf{Tasks:} They support professors in teaching and research. Some give their own lectures. 
In addition, they take on administrative and organisational tasks, e.g. in the area of study matters. 
\item \textbf{Where to find:} You will meet them, for example, in tutorials or central exercises as tutorial guides and tutorials. 
From an administrative point of view, you can meet them in the area of academic matters, for example, at the Student Advisory Service, the Examinations Office or the International Office.
\item \textbf{Form of address:} As these are often doctoral or postdocs who are on average only a few years older than you,
it is customary to be on first name terms. However, if you think that the age difference is greater, we recommend \enquote{Mr/Mrs \textless surname\textgreater}.
\end{itemize}


\subsection*{Tutorika} are employed as student or research assistants during the semester to support courses.
\begin{itemize}
\item \textbf{Classification:} As a rule, these are experienced students, 
who attended the lexture themselves a few semesters ago.
In addition, you will also find doctoral students and postdocs who are doing their teaching duties or earning a few euros on top of their salary.
\item \textbf{Tasks:} The range of tasks varies depending on the exercise system, but usually includes correcting exercise sheets, preparing and holding the tutorial, exchanging information between the student and the lecturer and correcting exams.
\item \textbf{Where to find:} You can find them tutorials and exercises.
\item \textbf{Areas:} see Scientific employees
\end{itemize}


\subsection*{Kommilitonika} is a common university term for your fellow students.
\begin{itemize}
\item \textbf{Classification:} Fellow students are students like you. Especially at our university, the spectrum is quite wide; from early studies to studies with a child to studies for senior citizens.
\item \textbf{Tasks:} As a (co-)Studikon you work independently. In principle, you can complete your studies on your own. However, we advise you to get in touch with your fellow students as early as possible so that you can devote yourself to your studies together. Working and discussing together has proven to be both productive and fun.
\item \textbf{Where to find:} You can find your fellow students almost everywhere around you, 
especially in lectures, tutorials, the library, in Café Gumbel, and of course at the O-Phase and other exchange events organised by the student council.
\item \textbf{Address:} Since the students are often of the same age as you, it is customary to use the first name in conversation.
\end{itemize}

\subsection*{Fachschaftika} are students like you, who, in addition to their studies, are involved as student representatives in the student body.
\begin{itemize}
\item \textbf{Classification:}
it
The Fachschaftika can be involved in different fields of activity, from university politics to the organization of festivals.
You can find a current list of the working groups in the next chapter \ref{gafaktionen}.
Many of the active Fachschaftika were elected via the university election of Studika.
\item \textbf{Tasks:} They represent student interests and opinions within the universities. In addition, Fachschaftika advocate better study conditions both within and outside of studies, e.g. by organising the O-Phase and exchange events. 
\item \textbf{Where to find:} In the lecture or library next to you. You can often find them in the student council room, room B038 in Theresienstr. 39 (Mathebau), and also by e-mail.
\item \textbf{Form of address:} Fachschaftika are students like you, so be on first-name terms!
\end{itemize}

\section{Student Participation}
As you have already seen, the structures of the university are quite complex.
One can quickly get the idea that we as Studika can hardly change anything about our studies.
That is not (quite) right! Because there are committed Studika members on almost all of the university's decision-making bodies.\\
Overall, we can distinguish between two major areas: firstly, the \textbf{Students' Representative Body} (StuVe), which operates on a cross-disciplinary basis at university level, 
and the \textbf{Fachschaft}, which is responsible for the studies of its subject at faculty level. 
Below you will find a brief overview of the StuVe, while the Fachschaft is explained in the next chapter \ref{diegaf}.

\subsection*{University-wide Committees}
With the \textit{Senate} and \textit{Hochschulrat}, Studika are represented in the university's highest bodies. They issue study regulations, elect the university management and adopt the basic regulations. Other university-wide committees which are co-designed by Studika are, for example, the \textit{Senat} and \textit{Hochschulrat}:
\begin{itemize}
\item University Strategy Committees: Extended Hoschool Management (EHL), Strategy Committee, 
\item Allocation of funds: Central Study Grant Commission (ZSK),
\item Development and assurance of the quality of teaching: Committee for Teaching and Studies (ALS).
\end{itemize}
The student representatives in the central bodies are delegated by the Council of the Student Associations.


\begin{figure}[h!]{
\begin{center}{

\resizebox{\linewidth}{!}{

\begin{tikzpicture}[scale=0.9]
\pie[sum=auto, radius=1, color={yellow!20, red!50}] {16/, 2/}
\node[above] at (0,1) {Senat};
\pie[pos={3,0}, sum=auto, radius=1, color={yellow!20, red!50}]{18/, 2/}
\node[above] at (3,1) {Hochschulrat};
\pie[pos={6,0}, sum=auto , after  number=, radius =1, color={yellow!20, red!50}]{26/, 1/ }
\node[above] at (6,1) {EHL};
\pie[pos={9,0}, sum=auto, radius=1, color={yellow!20, red!50}]{10/, 1/}
\node[above] at (9,1) {Strategie-Auss.};
\pie[pos={0,-3}, sum=auto, radius=1, color={yellow!20, red!50}]{6/, 6/}
\node[above] at (0,-2) {ZSK};
\pie[pos={3,-3}, sum=auto, radius=1, color={yellow!20, red!50}, text=legend]{9/ other interest groups, 4/ Studika}
\node[above] at (3,-2) {ALS};
\end{tikzpicture}
}\caption*{Share of students with voting rights in university-wide committees}
}\end{center}
}\end{figure}



\subsection*{Council of the Student Associations}
The \textit{Council of the Student Associations} consists of appointed representatives from the student association councils and serves as the supreme decision-making body of the StuVe.
It addresses the concerns of approximately 50,000 students at LMU and takes a stance on topics such as the semester ticket, cafeteria opening hours, or a bicycle workshop.
The Council is led by an elected chairperson and meets every second Wednesday during the semester. The \textit{management}, elected by the Council, acts as the executive body, implements the decisions of the Council, and manages the ongoing affairs of the StuVe.

\subsection*{Departments}
In the \textit{departments} Studika work on behalf of the Council on projects to improve student life.
The departments cover topics such as studies, environment, teaching, equality, culture or anti-fascism.
Each of the departments is headed by a Studikon, who is elected by the Council of Student Representatives. 
Anyone interested is welcome to participate, no election is required.\\\\

Further information on the StuVe, in particular the Council, its bodies and papers can be found online.
\begin{urlList}
	\urlItem{https://www.stuve.uni-muenchen.de/stuve/index.html}
\end{urlList}
