\chapter{Fachschaft -- die studentische Vertretung} \label{diegaf}

\section{Was ist die Fachschaft?}

Bei einer Fachschaft handelt es sich im Grunde um eine Gemeinschaft aus allen Studika eines Studiengangs -- somit ist auch jeder von euch Mitglied.

\begin{minipage}{6.5cm}
Wenn wir hier über \emph{die Fachschaft} sprechen, meinen wir damit aber meist die Gruppe Aktiver Fachschaftika, kurz GAF.
Dabei handelt es sich um eine Gruppe von Studika, die sich in verschiedenen Bereichen engagieren, um die Uni für sich und
ihre Mitstudika lebenswerter zu machen. In der GAF sammeln sich die aktiven Fachschaftika aus den Bereichen Mathematik,
Physik und Informatik, sowie der verwandten Fächer (Medieninformatik, Wirtschaftsmathematik, \ldots ).
\end{minipage}
\begin{minipage}{6cm}
\begin{center}
{\includegraphics[width=0.7\textwidth]{fachschaft}}
\end{center}
\end{minipage}\\

Für alle entsprechenden Studika ist die GAF also der Anlaufpunkt bei studienbezogenen Fragen und Nöten, aber auch wenn ihr euch selbst einbringen möchtet.

\section{Was macht die Fachschaft?} \label{gafaktionen}

Die Aufgaben der Fachschaft lassen sich grob in drei Bereiche einteilen: Veranstaltungen, Hochschulpolitik und direkte Unterstützung der Studika.
Manches bemerkt man unmittelbar im Alltag wie z.B. die Feste im Mathebau. Anderes bleibt
dagegen im Hintergrund verborgen, aber alle Bereiche sind wichtig, damit das Unileben
sich so angenehm wie möglich gestaltet und die Rechte der Studika gewahrt bleiben.
Im Nachfolgenden findet ihr einen kleinen Einblick in unsere Fachschaftsarbeit.

\subsection{Veranstaltungen}

Die GAF organisiert diverse Veranstaltungen des studentischen Lebens -- der ersten und wichtigsten, der \emph{O-Phase} für Erstsemestika, seid ihr
wahrscheinlich bereits begegnet. Hier werden neue Studika an die Universität und ihre Studiengänge herangeführt und bekommen Einblicke in die ungewohnte Umgebung.
Aber auch während des Semesters gibt es oft jede Menge zu tun:

\begin{itemize}
\item Beim \emph{Fakultätsfest} kommen die Studika zusammen und grillen im Sommer gemeinsam vor dem Mathebau.
\item Das \emph{Profcafé} gibt die Gelegenheit, sich mit Professorika in entspannterem Rahmen bei Käse und Wein zu unterhalten.
\item Bei der \emph{Langen Nacht der Universitäten} halten Professorika bis in den Morgen hinein interessante und lustige Vorträge über ihre Fachbereiche.
\item Der \emph{Spieleabend}, das \emph{Schafkopftunier} oder der \emph{Tanzabend} eignen sich perfekt, um andere Studika kennenzulernen und entspannt Zeit zu verbringen.
\item \ldots
\end{itemize}

\subsection{Hochschulpolitik}

Eine weitere wichtige Aufgabe der Fachschaft ist es, der studentischen Gemeinschaft ihrer Studiengänge in der Hochschulpolitik eine Stimme zu verleihen.
Es existieren zum Beispiel an unseren beiden Fakultäten verschiedene Gremien, in denen
wichtige Entscheidungen getroffen werden.
Jedes Sommersemester finden die Hochschulwahlen statt, bei denen ihr die Studika, die euch vertreten, wählen könnt. Die so gewählten Fachschaftsvertretungen ensenden dann Studis in die verschiedenen Gremien der Uni. Diese Studis haben dann ein Mitspracherecht
in den Gremien, verleihen den Meinungen und Interessen der Studika Gewicht und versuchen so, Entscheidungen über ihre Köpfe hinweg zu unterbinden.

Einige der wichtigsten Gremien haben wir hier aufgelistet:

\begin{itemize}
\item Der \emph{Fakulträtsrat} entscheidet alles Wichtige innerhalb der Fakultät und ist Ort des Informationsaustausches zwischen Professika, wissenschaftlichen Mitarbeitika und uns Studika. 
\item In den \emph{Lehrkommissionen} arbeiten Studika Empfehlungen zur Verbesserung von Lehre und Studium aus.
\item Die \emph{Berufungskommissionen} bestimmen, wer als neues Professorikon an unsere Uni kommt und hier auch lehrt.
\item Die \emph{Studienzuschusskommissionen}  entscheidet wie die Gelder aus den Studienzuschüssen zur Verbesserung von Lehre und Studium ausgegeben werden sollen.
\item Der \emph{Konvent der Fachschaften} besteht aus Vertretungen aller Fachschaften und beschäftigt sich mit fächerübergreifenden studentischen Themen wie dem Semesterticket.
\end{itemize}


\begin{figure}[h!]{
\begin{center}{

\resizebox{\linewidth}{!}{

\begin{tikzpicture}[scale=0.9]
\pie[sum=auto, radius=1, color={yellow!20, red!50}] {14/, 2/}
\node[above] at (0,1) {Fakultätsrat};
\pie[pos={3,0}, sum=auto, radius=1, color={yellow!20, red!50}]{6/, 6/}
\node[above] at (3,1) {Studienzuschuss};
\pie[pos={6,0}, sum=auto , after  number=, radius =1, color={yellow!20, red!50}]{10/, 4/ }
\node[above] at (6,1) {Lehrkom.};
\pie[pos={9,0}, sum=auto, radius=1, color={yellow!20, red!50}, text=legend]{8/ weitere Interessensgruppen, 1/ stimmberechtigte Studika}
\node[above] at (9,1) {Berufungskom.};
\end{tikzpicture}
}\caption*{Anteil an stimmberechtigten Studika in den Gremien innerhalb der Fakultät}
}\end{center}
}\end{figure}


\subsection{Allgemeine Unterstützung}

Im Allgemeinen versucht die GAF außerdem, den Studika das Unileben so leicht wie möglich zu machen.
Wir stehen als Ansprechpartnika für Probleme jeglicher Art zur Verfügung, 
im Fachschaftszimmer, per Mail oder Telefon (siehe Abschnitt \ref{gafKontakt}). Egal ob du Probleme in Vorlesungen,
Fragen zum Studium oder Anregungen, wie das Studium verbessert werden kann, hast:
Komm einfach vorbei oder melde dich.

\subsection{Altklausuren- und Skriptensammlung}

Ein sehr beliebter Dienst der GAF ist die Sammlung von Altklausuren und mündlichen Prüfungsprotokollen -- spätestens in der ersten Klausurenphase lernt
man, sie zur Prüfungsvorbereitung zu schätzen. Den größten Teil der Sammlung findest du in unserer Online-Sammlung~\ref{klausuren}. Benutzername und Passwort kannst du bei uns in der GAF erfragen oder dir per
E"~Mail~\ref{daten} zuschicken lassen. 

Auf unserer Sammelseite gibt es auch Mitschriften zu Vorlesungen, Prüfungsprotokolle zum Staatsexamen und zur Zwischenprüfung, sowie noch alte Protokolle aus Diplomzeiten.
Wichtig ist, dass wir vorerst nur selbstgeschriebene Mitschriften sammeln, und insbesondere keine offiziellen Skripte der Lehrpersonen.

Bei der Sammlung ist auch der Generationenvertrag zu beachten: sie existiert nur, weil ältere Studika ihre Prüfungen und Skripte zu uns gebracht haben,
also tu dies wenn möglich auch für deine Nachfolgenden, damit unsere Archive so vollständig wie möglich sind. Schicke ganz einfach alles, was du in die Hände
bekommst, sofern noch nicht vorhanden, an \mail{klausuren@fs.lmu.de}.

Selbst wenn die Professorika die Angaben nach der Prüfung wieder einsammeln, hast du das Recht, deine Klausur später beim Einsichtstermin zu fotografieren.
Sollte dir dies verwehrt werden, kannst du dich auf den Ministerialbeschluss \enquote{Weiterentwicklung des Bologna-Prozesses, insbesondere Prüfungsrecht} beziehen,
in dem explizit das Ablichten deiner Klausur erlaubt wird. Die Nächsten werden es dir danken! 

\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de/klausuren}[klausuren]
	\urlItem{https://gaf.fs.lmu.de/rund-ums-studium/zugangsdaten}[daten]
\end{urlList}

\subsection{Das Café Gumbel}

Ein weiterer wichtiger Teil der Fachschaftsarbeit ist der Betrieb und Erhalt des Café Gumbel.
Hierbei handelt es sich um einen studentischen Aufenthaltsraum mit Küche in der Theresienstraße 37--39 (Raum B\,030).

Das Café Gumbel, vor Jahrzehnten von Studika erstreikt, wird seit vielen Jahren von der Fachschaft verwaltet, die es wiederum den Studika zur Verfügung stellt.
Neben Tischen zum Arbeiten, gemütlichen Sofas zum Entspannen und einem Klavier stehen hier auch Wasserkocher und Mikrowelle zur freien Verfügung.
Natürlich gibt es in der Küche Regeln zu Ordnung und Sauberkeit sowie zum vorsichtigen Umgang mit Schränken und Geräten. Hier gilt es, alles so zu hinterlassen, wie man
es auch gerne vorfinden würde, und fremdes Eigentum pfleglich zu behandeln.
Sollte es doch mal ein Problem geben, melde dieses bitte der Fachschaft, damit wir uns darum kümmern können.

Da das Gumbel für viele Studika Dreh-~und Angelpunkt ist, wird hier und in anderen Uniräumen einiges an persönlichen Gegenständen vergessen. Damit du eine Chance hast,
deine Sachen wiederzubekommen, beschriftest du am besten alles mit Namen und Telefonnummer. Falls doch mal etwas abhanden kommen sollte, frag am besten im Mathebau
an der Pforte oder im Fachschaftszimmer nach, dort werden gelegentlich verlorene Gegenstände angespült.

Ab und zu wird das Café Gumbel auch für studentische Veranstaltungen genutzt, von denen einige schon unter dem Punkt Veranstaltungen aufgelistet wurden, wie etwa der Spieleabend.
Wenn du Fragen zum Gumbel oder Veranstaltungen im Gumbel hast, erreichst du die Verantwortlichen über \mail{gumbel@fs.lmu.de}.

\begin{minipage}{6cm}
	 \textit{Das Café Gumbel ist übrigens nach dem Münchner Mathematiker und politischen Aktivisten Emil Julius Gumbel benannt, der sich mit Statistiken zu politisch motivierten Morden beschäftigte und sich gegen den Nationalsozialismus engagierte.}
\end{minipage}
\begin{minipage}{7cm}
\begin{center}
     \includegraphics[width=0.75\textwidth]{gumbel}\\
     Unser Studierendencafé
\end{center}
\end{minipage}

\section{Mitmachen in der Fachschaft}

Wenn du neugierig geworden bist, gerne mal einen Blick in die Arbeit der Fachschaft und hinter die Kulissen der Uni werfen würdest,
selbst gute Ideen für Veranstaltungen hast, bei Veranstaltungen
mithelfen und mitorganisieren oder in die Hochschulpolitik hineinschnuppern möchtest, bist du in der GAF immer herzlich willkommen!

Es gibt ganz verschiedene Arbeitskreise und Bereiche, in denen mitgemacht werden kann, da ist für alle was dabei. Es handelt sich natürlich
um ehrenamtliche Arbeit; das Geld, das uns zur Verfügung steht, setzen wir also nur zugunsten der Studika ein und der einzige Lohn sind mehr
Lebenserfahrung und in manchen Fällen ein verlängertes Studium.

\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de/fachschaft}
\end{urlList}

\subsection*{Das Erstiwochenende}

Das Erstiwochenende, kurz EWO, ist eine Veranstaltung extra für Erstis, bei der ihr gemeinsam mit erfahrenen GAFika und neuen Studika für ein Wochenende
wegfahrt, euch kennenlernt, Spaß habt und Einblicke in die Arbeit der Fachschaft bekommt. Es ist die perfekte Gelegenheit, um Kontakte zu knüpfen und
ein bisschen in neue Gebiete reinzuschnuppern.
Wenn ihr interessiert seid, meldet euch am besten direkt an, denn es gibt nicht immer genug Plätze.

\subsection*{Die Fachschaftssitzung}

Alle zwei Wochen findet die Fachschaftssitzung statt, bei der sich GAFika und Interessierte treffen, um aktuell wichtige Themen zu diskutieren und Entscheidungen zu treffen.
Hier wird Neulingen auch immer gerne erklärt, was es gerade zu tun gibt. Komm einfach vorbei!

\subsection*{Thementreffen}

Neben der Fachschaftssitzung finden jeden Donnerstag und jeden zweiten Montag Thementreffen statt, wo über verschiedenste Themen geredet und an diesen gearbeitet wird. 
Die Thementreffen sind auch etwas weniger rigide strukturiert als die Fachschaftssitzung. 
Falls du also die Fachschaftsarbeit kennenlernen willst, bieten sie eine entspannte Atmosphäre dafür. 
Die genauen Termine werden auch immer in unserem Newsletter bekannt gegeben, Ort ist immer B038 im Mathebau (Theresienstr. 39).

zu Beginn des Semesters wird es außerdem eine Schnuppersitzung geben, in welcher verschiedene Bereiche der Fachschaft vorgestellt werden.

Im Nachfolgenden haben wir die verschiedenen Themenbereiche der Fachschaftsarbeit aufgelistet. An allen davon wird regelmäßig auf den Thementreffen gearbeitet.

\subsection*{O-Phase Orga}

Wenn du viel Spaß an der O-Phase hattest oder dir vielleicht Dinge aufgefallen sind, die du anders oder viel besser machen würdest, dann ist der AK O-Phase der richtige Ort für dich. 
Da die O-Phase unsere größte Veranstaltung ist, beginnen wir mit der Vorbereitung immer direkt nach der letzten O-Phase. 
Die Orga-Treffen für die O-Phase sind meistens alle zwei Wochen, Montags, abwechselnd zur Fachschaftssitzung.
Hier kann man dazu beitragen, dass nachfolgende Generationen an Erstis einen guten Einstieg ins Studium erleben und sammelt gleichzeitig Erfahrung damit, groß angelegte Veranstaltungen zu planen und umzusetzen.

Fragen können an \mail{ophase@fs.lmu.de} geschickt werden.

\subsection*{Hochschulpolitik (HoPo)}

Zu bestimmten Thementreffen finden regelmäßig fachspezifische (Mathe, (Medien)Informatik und Physik) oder fachübergreifende HoPo-Sitzungen statt.
In diesen werden aktuelle Themen und Probleme besprochen und das weitere Vorgehen geplant. Häufige themen sind die hochschulpolitischen Gremien, Probleme mit Veranstaltungen oder Dozentika, die Kommunikation mit Professika, die Verbesserung der Studiensituation und vieles mehr. Wenn du dich in dieser Richtung engagieren möchtest, komm einfach zu einem dieser Thementreffen. 

\subsection*{Feste und Veranstaltungen}

Wie oben beschrieben, organisiert die GAF, neben der O-Phase, viele verschiedene Veranstaltungen. Wenn du Lust hast bei einer Veranstaltung mitzuhelfen oder eine Veranstaltung mit zu organisieren, komm einfach vorbei. Auch Vorschläge und Ideen für neue Veranstaltungen sind jederzeit willkommen.

Fragen können auch an \mail{gaf@fs.lmu.de} geschickt werden.

\subsection*{Kommunikation}

Um euch zu erreichen, nutzen wir viele verschiedene Kommunikationskanäle wie z.B. den Klostein, den Newsletter oder unsere Social Media Accounts.

\subsection*{AK root}

Im AK root können sich Technik- und Informatik-Interessierte ausleben. Er kümmert sich um die Server und Dienste der GAF sowie einiger anderer Fachschaften, wie zum Beispiel
die Mailinglisten oder die Fachschaftshomepage, sowie die Instandhaltung der Hardware und bei gelegentlichen Hackingnights auch größere anfallende IT-Themen.

Bei Interesse einfach eine Mail an \mail{root@fs.lmu.de} schicken.

\section{Kontakt}\label{gafKontakt}

\begin{urlList}
	\urlItem{https://gaf.fs.lmu.de}[gaf]
\end{urlList}


\begin{figure}[h]
	\centering
	{\includegraphics[width=0.8\textwidth]{GAF_Gruppenfoto_2023}}
	
	%% UPDATE 2024: aktuelles Fachschaftsbild
	
\end{figure}

\begin{center}
\textbf{Kontakt:} \\
Theresienstraße 37, Raum B038 \bullet~089 / 2180\emd{}4382 \bullet~\mail{gaf@fs.lmu.de}\\
\url{\https gaf.fs.lmu.de} \bullet~\url{\https instagram.com/gaflmu/}
\end{center}

\iffalse
\begin{figure}[h]

\begin{tabular}{|l l| l l}
\hline	
Zimmer&Theresienstraße 37\\
&Raum B038\\
Telefon&089 / 2180\emd{}4382\\
Telefax&089 / 2180\emd{}99 4382\\
&\mail{gaf@fs.lmu.de}\\
&\url{\https gaf.fs.lmu.de}\\
&\url{\https instagram.com/gaflmu/}\\
IRC & \#gaf auf libera.chat \\ \hline
\end{tabular}

\end{figure}
\fi



