# Workflow

## TLDR

Eine Kommandozeile öffnen und `make` eingeben

## Build

`make` den deutschsprachigen Einstein erstellen, Ergebnis `main.pdf`  
`make english` den englischsprachigen Einstein erstellen, Ergebnis `main-english.pdf`  
`make clean` Hilfsdateien löschen  
`make distclean` Hilfsdateien und Einstein-pdf (`main.pdf` bzw. `main-english.pdf`) löschen  

Das Erstellen des Ersti-Einsteins läuft in den folgenden vier Schritten ab:
 1. Inhalt erstellen
 Für den deutschsprachigen Einstein im Ordner `german/`, für den englischsprachigen Einstein im Ordner `english/`  
 In den beiden Ordnern müssen Dateien mit dem selben Namen sein. Diese werden dann von der Datei `main.tex` eingebunden.  
 Check: Rechtschreibung, Grammatik, Inhalt, ...
 2. An LaTeX Automatismen anpassen  
 Check: Optik, Zeilenumbruch, Seitenanzahl ist Vielfaches von 4
 3. Drucken
 4. Aufräumen


_An LaTeX Automatismen anpassen_ beinhaltet vor allem die folgenden drei Punkte  
 a. Over- und underfull boxes ausgleichen  
 b. Automatische Worttrennung kontrollieren/korrigieren  
 c. Blockweise umstrukturieren

Diese Punkte werden im ersten run noch in dieser Reihenfolge abgearbeitet, jedoch kann eine Änderung die Wiederholung der Schritte a-c nach sich ziehen. Eine Änderung wirkt sich zum Glück nur auf den gesamten Text ab dieser Änderung aus. Eine Art Savepoint ist ein erzwungener Seitenwechsel, der zum Beispiel durch ein neues Kapitel eingeleitet wird. Das heißt, die Änderungen wirken sich nur bis dahin aus. Hat eine Änderung zur Folge, dass sich die Seitenzahl bis zum nächsten Savepoint ändert, kann das Auswirkungen auf die Anordnung aller folgenden Seiten bis zum Ende des gesamten Dokuments haben.

Hinweis: Im Allgemeinen ist davon abzuraten, größere inhaltliche Änderungen kurz vor dem Druck vorzunehmen, da diese wahrscheinlich nicht rechtzeitig Korrektur gelesen werden können.

# Texting Style


* Möglichst ganze Sätze bilden (Abkürzungen durch Stichwortlisten vermeiden).
* Für Geldbeträge `\EUR{<Zahl>}` statt € verwenden.
* Von...bis Angaben im Fließtext ausschreiben. Im Fall von Zahlen, Daten oder Ähnlichem wird `--` ohne Leerzeichen genutzt (zum Beispiel `3--5`, `Mo--Fr`).
* Zwischen Abkürzungen gehört ein dünnes Leerzeichen: `z.\,B.` (oder gleich ausschreiben)

# ToDo

  * Updates (dringend):
    * [ ] alle Links auf Funktionalität prüfen (einfach Kurzlinks durchgehen)
    * [ ] mit updateMe.sh nach UPDATEs suchen die noch nicht aktualisiert wurden und Daten checken; das Jahr hinter UPDATE auf das aktuelle setzen
    * [ ] für die neue Auflage erstmal alle /newpage rausnehmen und gegebenenfalls erst ganz am Ende im layout wieder einfügen
  * siehe Issues: Vorschläge für die Zukunft (nicht dringend)
  * Typographie/Inhalt (nicht dringend)
    * [ ] Over-/Underfull boxes entfernen
    * [ ] Sonstige warnings entfernen
  * Code Aufräumen (nicht dringend)
    * [ ] Einheitlicher coding style (wie viele leerzeichen vor/nach Überschriften verschiedener Art, Bilder, ...)
    * [ ] \usepackage ausmisten
    * [ ] Richtiges/Einheitliches Einrücken (1 Tab)
    * [ ] Ein (Neben-)Satz = Eine Zeile (kein 80 Zeichen Umbruch!)
    * [ ] Windows newlines fixen (in den Dateien und mit .gitattributes)
    * [ ] Ausmisten
  * GIT (nicht dringend)
    * [ ] Irgendwie mit ophase repo verbinden
    * [ ] Git history mit altem Einstein repo verbinden
    * [ ] Hier müssten noch ein paar große zip und sonstige große unnötige Dateien in der history rumflacken -> rausfiltern
  * [ ] Automatischer upload auf die Webseite
  * [ ] Automatisches update der Webcodes
  * [ ] Diese Liste um alle Punkte aus den handschritlichen Annotationen aus dem Einstein-Fach ergänzen
  * [ ] Optionale 3te (oder 2te) Notizen Seite, die also ein Bild hat, aber nur erscheint, wenn noch eine Seite zum \cleardoublepage fehlt
  * [ ] Reproduzierbare builds
