DAY_FILES   := $(wildcard days/*.tex)
SVGS        := $(wildcard img/*.svg)
IMG_PDFS    := $(SVGS:img/%.svg=tmp/img/%.pdf)


## Target if make is invoked without any parameter (goal)
.DEFAULT_GOAL: all

## "Virtual" targets without actual files to update/create
.PHONY: all german english inkscape clean distclean new

## german einstein
all: german english

## english einstein
german: ersti_einstein-german.pdf
english: ersti_einstein-english.pdf

FORCE:
	@true

#Kalendar geht von 1 Woche vor Unibeginn bis Ende Semesterferien Winter
tmp/kalender-days.tex: createcal.py week.tpl $(DAY_FILES)
	mkdir -p tmp
	env LANG=de_DE.UTF-8 python3 createcal.py >tmp/kalender-days.tex


.ONESHELL:
tmp/%.pdf: tmp/kalender-days.tex $(IMG_PDFS) FORCE
	@export max_print_line=1000
	export error_line=254
	export half_error_line=238
	export FONTCONFIG_FILE=`pwd`/.fonts.conf
	export SOURCE_DATE_EPOCH=0
	
	## When is this needed? I currently can't reproduce the problem it solved.
	luaotfload-tool --update -v
	
	## need to create directory for *.aux files
	EINSTEINLANG="$$(basename "$@" .pdf)"
	mkdir -p "tmp/$${EINSTEINLANG}"
	
	if ! latexmk -jobname="$${EINSTEINLANG}" ; then
		grep  -H -C 10 --color=always -n -E '^(\./|\!).*' "tmp/$${EINSTEINLANG}.log" | \
		python3 -c 'import sys;print(sys.stdin.read().replace(sys.argv[1], sys.argv[2]))' "tmp/$${EINSTEINLANG}.log" '' | \
		python3 -c 'import sys;print(sys.stdin.read().replace(sys.argv[1], sys.argv[2]))' $$'\033[K./' "$$PWD/"
		
		rm -fv "tmp/$${EINSTEINLANG}.pdf"
		exit 1
	fi
	
	if [ 0 -ne $$(expr $$(sed -n 's/^Output written on [^(]*(\([0-9]*\) pages.*/\1/p' "tmp/$${EINSTEINLANG}.log") % 4) ] ; then
		echo -e "\033[1;33;41m                                                  \033[0m" >&2
		echo -e "\033[1;33;41m /!\ ERROR: Page count is not a multiple of 4 /!\ \033[0m" >&2
		echo -e "\033[1;33;41m                                                  \033[0m" >&2
		exit 1
	fi


$(IMG_PDFS): tmp/img/%.pdf: img/%.svg
	@mkdir -p tmp/img
	## different commands in different inkscape versions
	## delete in 2022 at latest, update CI before
	if inkscape --help 2>/dev/null | grep -q -e --export-filename ; then
		## Inkscape 1 syntax
		FONTCONFIG_FILE=`pwd`/.fonts.conf inkscape --export-area-page --export-filename $@ $<
	else
		## Inkscape <1 syntax
		FONTCONFIG_FILE=`pwd`/.fonts.conf inkscape --export-area-page --export-pdf $@ $<
	fi


inkscape:
	FONTCONFIG_FILE=`pwd`/.fonts.conf inkscape

ersti_einstein-%.pdf: tmp/%.pdf
	ln -f $< $@


clean:
	rm --force --verbose --recursive \
		tmp


distclean: clean
	rm --force --verbose \
		ersti_einstein-german.pdf \
		ersti_einstein-english.pdf


new: distclean
	$(MAKE)
