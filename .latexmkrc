## latexmk line 1640: Set xelatex
$pdflatex = "texfot lualatex %O %S";
$pdf_mode = 1;
$dvi_mode = 0;
$postscript_mode = 0;

$out_dir = "tmp";
$aux_dir = "tmp";
@default_files = ( "main.tex" );
#$silent = 1;

push @extra_latex_options,    "-shell-escape";
push @extra_pdflatex_options, "-shell-escape";
push @extra_latex_options,    "-file-line-error";
push @extra_pdflatex_options, "-file-line-error";
push @extra_latex_options,    "-8bit";
push @extra_pdflatex_options, "-8bit";
push @extra_latex_options,    "-halt-on-error";
push @extra_pdflatex_options, "-halt-on-error";

$ENV{'TEXMFLOCAL'}='cd/texmf/:' . ($ENV{'TEXMFLOCAL'} || '');
